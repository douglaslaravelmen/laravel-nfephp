<?php

Route::group(['namespace' => 'douglasmen\LaravelPhpNfe\Http\Controllers', 'middleware' => ['web']], function() {
    Route::get('/gerar-danfe', 'NfeController@gerarDanfe');
});
