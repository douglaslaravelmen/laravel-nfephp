<?php
namespace douglasmen\LaravelPhpNfe;

use Illuminate\Support\ServiceProvider;

class LaravelPhpNfeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
    }

    public function register()
    {

    }
}
