<?php
namespace douglasmen\LaravelPhpNfe\Services;

require __DIR__.'/../../vendor/autoload.php';

use NFePHP\Extras\Danfe;
use NFePHP\Common\Files\FilesFolders;

class NfePhp 
{   
    public function mostrarDanfeBrowser($nome_xml)
    {   
        $loaded_xml = FilesFolders::readFile($nome_xml);
        $danfe = new Danfe($loaded_xml, 'P', 'A4', '', 'I', '');
        $hash_danfe = $danfe->montaDANFE();
        
        return $danfe->printDANFE($nome_xml);
    }    
}
