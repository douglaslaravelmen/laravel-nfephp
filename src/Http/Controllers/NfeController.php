<?php
namespace douglasmen\LaravelPhpNfe\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use douglasmen\LaravelPhpNfe\Services\NfePhp;

class NfeController extends Controller
{
    // public function gerarDanfe($nome_xml = __DIR__."/../../teste.xml")
    public function gerarDanfe(Request $request)
    {   
        $php_nfe = new NfePhp;
        return $php_nfe->mostrarDanfeBrowser($request->nome_xml);
    }
}
